package com.djfm.performancesystem.services

import com.djfm.performancesystem.exceptions.BadRequestException
import com.djfm.performancesystem.exceptions.ForbiddenRequestException
import com.djfm.performancesystem.models.Workout
import com.djfm.performancesystem.repositories.WorkoutRepository
import com.djfm.performancesystem.responses.WorkoutResponse
import org.springframework.stereotype.Service

@Service
class WorkoutService(private val workoutRepository: WorkoutRepository) {
    fun getAllWorkouts(): MutableIterable<Workout> {
        return workoutRepository.findAll()
    }

    fun getWorkoutById(workoutId: Long): Workout{
        val wod = workoutRepository.findById(workoutId)
        return when {
            wod.isPresent -> wod.get()
            else -> throw BadRequestException("Wod does not exist")
        }
    }

    fun addWorkout(workout: Workout): Workout{
        val wod = workoutRepository.findById(workout.id)
        return when {
            wod.isPresent -> {
                val wodId = wod.get().id
                workoutRepository.deleteById(wodId)
                workoutRepository.save(workout)
            }
            else -> workoutRepository.save(workout)
        }
    }

    fun updateWorkout(workout: Workout, workoutId: Long): Workout {
        return workoutRepository.save(workout)
    }

    fun deleteWorkout(workoutId: Long): WorkoutResponse{
        workoutRepository.deleteById(workoutId)
        val deletedWorkout = workoutRepository.findById(workoutId)
        when {
            deletedWorkout.isPresent -> throw BadRequestException("It seems that the resource that you've requested to delete still exists this is likely because you do not have permission to delete it!")
            else -> return WorkoutResponse("Successfully deleted workout!", workoutId)
        }
    }
}