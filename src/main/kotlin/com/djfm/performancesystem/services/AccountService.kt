package com.djfm.performancesystem.services

import com.djfm.performancesystem.exceptions.BadRequestException
import com.djfm.performancesystem.models.Account
import org.springframework.stereotype.Service


/*
@Service
class AccountService(private val accountRepository: AccountRepository) {
    fun getAccountById(id: Long): Account {
        val account = accountRepository.findById(id)
        return when {
            account.isPresent -> account.get()
            else -> throw BadRequestException("Account doesn't exist")
        }
    }

    fun addAccount(account: Account): Account {
        return accountRepository.save(account)
    }
}
*/