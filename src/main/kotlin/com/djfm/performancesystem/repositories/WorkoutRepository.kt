package com.djfm.performancesystem.repositories

import com.djfm.performancesystem.models.Workout
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface WorkoutRepository : CrudRepository<Workout, Long>