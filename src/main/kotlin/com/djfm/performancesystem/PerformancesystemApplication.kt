package com.djfm.performancesystem

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.retry.annotation.EnableRetry


@SpringBootApplication
@EnableRetry
open class PerformancesystemApplication

fun main(args: Array<String>) {
    SpringApplication.run(PerformancesystemApplication::class.java, *args)
}
