package com.djfm.performancesystem.controllers

import com.djfm.performancesystem.exceptions.BadRequestException
import com.djfm.performancesystem.models.Workout
import com.djfm.performancesystem.responses.WorkoutResponse
import com.djfm.performancesystem.services.WorkoutService
import org.springframework.web.bind.annotation.*

@RestController()
@RequestMapping("/api/workout")
class WorkoutController(private val workoutService: WorkoutService) {

    @GetMapping
    fun getAllWorkouts(): MutableIterable<Workout>{
        return workoutService.getAllWorkouts()
    }

    @GetMapping("{workoutId}")
    fun getWorkoutById(@PathVariable workoutId: Long): Workout{
        return workoutService.getWorkoutById(workoutId)
    }

    @PostMapping
    fun addWorkout(@RequestBody workout: Workout): Workout {
        return workoutService.addWorkout(workout)
    }

    @PutMapping("{workoutId}")
    fun updateWorkout(@RequestBody workout: Workout, @PathVariable workoutId: Long): Workout {
        when {
            workout.id != workoutId -> throw BadRequestException("Path id and object id does not match")
            else -> {
                return workoutService.updateWorkout(workout, workoutId)
            }
        }
    }

    @DeleteMapping("{workoutId}")
    fun deleteWorkout(@PathVariable workoutId: Long): WorkoutResponse {
        return workoutService.deleteWorkout(workoutId)
    }
}