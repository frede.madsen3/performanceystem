package com.djfm.performancesystem.responses

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.OK)
data class WorkoutResponse(val message: String, val id: Long)