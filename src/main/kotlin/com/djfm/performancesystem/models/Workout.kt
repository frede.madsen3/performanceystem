package com.djfm.performancesystem.models

import javax.persistence.*

@Entity
data class Workout(
        var name: String = "",
        var description: String = "",
        var rating: Int = 0,

        /*
        @OneToMany(mappedBy = "workout", cascade = [(CascadeType.ALL)])
        var comments: MutableList<Comment> = mutableListOf(),

        @OneToMany(mappedBy = "workout", cascade = [(CascadeType.ALL)])
        var participants: MutableList<Participant> = mutableListOf(),

        @OneToMany(mappedBy = "workout", cascade = [CascadeType.ALL])
        var excersices: MutableList<Exercise> = mutableListOf(),
        */

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0
)