package com.djfm.performancesystem.models

import javax.persistence.*


@Entity
data class Participant (
        /*
        val name: String = "",

        @ManyToOne
        val workout: Workout,

        val stats: HashMap<String, Exercise> = hashMapOf(),
        */

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0
)