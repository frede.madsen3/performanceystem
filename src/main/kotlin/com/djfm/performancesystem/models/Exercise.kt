package com.djfm.performancesystem.models

import javax.persistence.*

@Entity
data class Exercise(
        /*
        @ManyToOne
        var workout: Workout = Workout(),

        @ElementCollection
        @Enumerated
        var repScheme: MutableList<ExerciseType> = mutableListOf(ExerciseType.NONE),

        var recommendedWeight: Int = 0,

        @OneToMany(mappedBy = "workout", fetch = FetchType.EAGER, cascade = [(CascadeType.ALL)])
        var scaledExercises: MutableList<Exercise> = mutableListOf(),
*/
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0
)