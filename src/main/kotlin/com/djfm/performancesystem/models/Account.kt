package com.djfm.performancesystem.models

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class Account(
/*
        val name: String = "",
        val description: String = "",

        //val personalRecords: HashMap<String, ExerciseType> = hashMapOf(),
*/
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0
)