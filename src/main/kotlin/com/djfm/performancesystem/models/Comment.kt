package com.djfm.performancesystem.models

import javax.persistence.*
import javax.persistence.FetchType

@Entity
data class Comment(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Int = 0
/*
        @ManyToOne
        var workout: Workout = Workout(),

        @ManyToOne(cascade = [(CascadeType.ALL)])
        var parentComment: Comment?,

        @OneToMany(mappedBy = "parentComment", fetch = FetchType.EAGER, cascade = [(CascadeType.ALL)])
        var childComments: MutableList<Comment> = mutableListOf()
        */
) {
/*
    override fun toString(): String {
        return "Comment(id=$id)"
    }
*/
}
