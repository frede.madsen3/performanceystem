package com.djfm.performancesystem.models


enum class ExerciseType {
    KG,
    REPS,
    TIME,
    DISTANCE,
    MAX,
    NONE
}
